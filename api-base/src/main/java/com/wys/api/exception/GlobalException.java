package com.wys.api.exception;

import com.wys.api.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
@RestController
@Slf4j
public class GlobalException {

    @ExceptionHandler(BizException.class)
    public R bizException(HttpServletRequest httpServletRequest, BizException e) {
        log.error("业务异常:", e);
        return R.fail(e.getMessage());
    }


}
