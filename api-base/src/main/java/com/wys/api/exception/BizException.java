package com.wys.api.exception;

import com.wys.api.common.BaseErrorCode;

public class BizException extends RuntimeException {

    private int status;

    private String message;

    public BizException(String message, Throwable cause) {
        super(message, cause);
    }

    public BizException(int status, String message) {
        super(message);
        this.message = message;
        this.status = status;
    }

    public BizException(BaseErrorCode baseErrorCode) {
        this.status = baseErrorCode.getStatus();
        this.message = baseErrorCode.getMessage();
    }

    public BizException(String message) {
        this.status = BaseErrorCode.SERVICE_UNAVAILABLE.getStatus();
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
