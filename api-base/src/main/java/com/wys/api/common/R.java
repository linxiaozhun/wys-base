package com.wys.api.common;

import com.wys.api.fun.RFunction;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.function.BiFunction;

@Getter
@Setter
@NoArgsConstructor
@ApiModel(value = "基础响应类")
public class R<T> {

    @ApiModelProperty(value = "接口响应状态",notes = "200：请求成功 500:请求失败" )
    private int status;

    @ApiModelProperty(value = "响应数据体",notes = "json体")
    private T data;

    @ApiModelProperty(value = "响应消息")
    private String message;


    public  R(int status, T o, String message) {
        this.status = status;
        this.message = message;
        this.data = o;
    }


    public static R buildR(int status, Object o, String message) {
        return new R(status, o, message);
    }

    public static R ok(int status, String message) {
        return buildRT.apply(status, message, null);
    }

    public static R ok() {
        return buildRT.apply(CommonCode.OK, "请求成功", null);
    }

    public static R ok(String message, Object obj) {
        return buildRT.apply(CommonCode.OK, message, obj);
    }

    public static R ok(Object obj) {
        return buildRT.apply(CommonCode.OK, null, obj);
    }

    public static R ok(String message) {
        return buildRT.apply(CommonCode.OK, message, null);
    }

    public static RFunction<Integer, String,Object> buildRT = (in, str, o) -> buildR(in, o, str);



    public static R fail(int status, String message, Object obj) {
        return buildRT.apply(status, message, obj);
    }

    public static R fail(String message, Object obj) {
        return buildRT.apply(CommonCode.ERROR, message, obj);
    }

    public static R fail(int status, String message) { return buildRT.apply(status,message, null); }


    public static R fail(String message) {
        return buildRT.apply(CommonCode.ERROR, message, null);
    }

    public static  R fail() {
        return buildRT.apply(CommonCode.ERROR, "请求失败", null);
    }


}
