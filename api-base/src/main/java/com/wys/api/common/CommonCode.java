package com.wys.api.common;

public interface CommonCode {

    /*=========http状态码============*/

    int OK = 200;

    int ERROR = 500;

    int UNAUTHORIZED = 401;


    /*======数据删除状态==========*/

    int DEL_YES = 1;

    int DEL_NO = 0;


}
