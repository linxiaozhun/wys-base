package com.wys.api.funinterface;

import javax.validation.groups.Default;

/**
 * <p>
 * 添加类的分组接口
 * </p>
 *
 * @author 欧阳
 * @Version: V1.0
 * @since 2022/6/6 16:00
 */
public interface AddGroup extends Default {
}
