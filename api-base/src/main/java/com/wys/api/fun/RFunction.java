package com.wys.api.fun;


import com.wys.api.common.R;

@FunctionalInterface
public interface RFunction<I, U, O> {
     R<O> apply(I i, U u, O o);
}
