package com.wys.utils;

import cn.afterturn.easypoi.entity.vo.NormalExcelConstants;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.afterturn.easypoi.view.PoiBaseView;
import com.wys.api.common.R;
import org.springframework.ui.ModelMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * excel工具类
 *@author sansan
 *@createDate 2022/10/26 11:17
 */

public class ExcelUtils {
    /**
     *  @Param:   title文档标题
     * @Param:  dataList需要导出的数据
     * @Param:  T t 传入的数据类型
     * @Param:  fileName 文件名
     * @return:
     */
    public static  final <T> void export(HttpServletRequest request,
                                         HttpServletResponse response, String title, List<T> dataList, Class<T> tClass, String fileName){
        ModelMap map = new ModelMap();
        ExportParams params = new ExportParams(title, title, ExcelType.XSSF);
        map.put(NormalExcelConstants.DATA_LIST, dataList);
        map.put(NormalExcelConstants.CLASS, tClass);
        map.put(NormalExcelConstants.PARAMS, params);
        map.put(NormalExcelConstants.FILE_NAME, fileName);
        PoiBaseView.render(map, request, response, NormalExcelConstants.EASYPOI_EXCEL_VIEW);
    }

}
