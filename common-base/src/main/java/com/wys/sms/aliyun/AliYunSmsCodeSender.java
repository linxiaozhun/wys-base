package com.wys.sms.aliyun;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.exceptions.ClientException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;



/**
 *阿里云短信验证码发送器
 *@author sansan
 *@createDate 2022/9/26 16:35
 */

@Slf4j
@Component("smsCodeSender")
public class AliYunSmsCodeSender  {
    public boolean send(String mobile, String code) {
        JSONObject param = new JSONObject();
        param.put("code",code);
        try {
            boolean isOk = AliYunSmsUtil.sendSms(mobile, AliYunSmsTemplate.VERIFICATION_CODE, param);
            if (isOk){
                log.info("发送验证码成功："+mobile+","+code);
            }
            return isOk;
        } catch (ClientException e) {
            log.error("发送短信异常：{}",e);
        }
        return false;
    }

}
