package com.wys.sms.aliyun;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.exceptions.ClientException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SmsCodeSender {
    public static boolean send(String mobile, String code) {
        JSONObject param = new JSONObject();
        param.put("code",code);
        try {
            boolean isOk = AliYunSmsUtil.sendSms(mobile, AliYunSmsTemplate.VERIFICATION_CODE, param);
            if (isOk){
                log.info("发送验证码成功："+mobile+","+code);
            }
            return isOk;
        } catch (ClientException e) {
            log.error("发送短信异常:",e);
        }
        return false;
    }
}
