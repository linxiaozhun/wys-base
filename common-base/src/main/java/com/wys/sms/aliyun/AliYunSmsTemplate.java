package com.wys.sms.aliyun;

/**
 *阿里云短信服务模板code
 *@author sansan
 *@createDate 2022/9/26 16:36
 */

public enum AliYunSmsTemplate {

    /**
     * 阿里云模板code
     */
    VERIFICATION_CODE("SMS_242620803","验证码"),
    ;

    AliYunSmsTemplate(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;
    private String desc;

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
