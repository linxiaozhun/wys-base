package com.wys.spring.mybatisplus.generator;


import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Sequence;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.wys.key.UUIDKeyGenerator;
import com.wys.utils.JsonUtils;
import org.springframework.core.annotation.AnnotationUtils;

import java.net.Inet4Address;

public class CustomLengthIdentifierGenerator implements IdentifierGenerator {

    public boolean assignId(Object idValue) {
        return StringUtils.checkValNull(idValue);
    }

    @Override
    public Number nextId(Object entity) {
        return new Sequence(Inet4Address.getLoopbackAddress()).nextId();
    }

    @Override
    public String nextUUID(Object entity) {
        System.out.println(JsonUtils.object2Json(entity));
        Key key = AnnotationUtils.findAnnotation(entity.getClass(), Key.class);
        if (ObjectUtils.isNotEmpty(key)) {
            return UUIDKeyGenerator.init(key.length(), UUIDKeyGenerator.Type.bytes).generatorKey();
        } else {
            return UUIDKeyGenerator.init(32, UUIDKeyGenerator.Type.shared).generatorKey();
        }
    }


}
