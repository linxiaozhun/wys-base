package com.wys.spring.mybatisplus.generator;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Documented
public @interface Key {

    /**
     * 设置生成的随机id长度 默认32位
     * @return
     */
    int length();

}
