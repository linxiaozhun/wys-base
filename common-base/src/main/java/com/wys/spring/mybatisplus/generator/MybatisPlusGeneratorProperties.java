package com.wys.spring.mybatisplus.generator;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties(prefix = "mybatis-plus.generator")
public class MybatisPlusGeneratorProperties {

    private String author;

    private String controller;

    private String service;

    private String outDir;

    private String mapper;

    private String entity;

    private String modelName;

    private String mapperXml;

    private String parentName;

    private String tableName;

    private String prefixTableName;
    //多个用逗号隔开
    private String tables;

    private String excludes;



}
