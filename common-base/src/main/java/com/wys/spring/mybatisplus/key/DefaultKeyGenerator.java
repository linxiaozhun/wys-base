package com.wys.spring.mybatisplus.key;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.incrementer.IKeyGenerator;
import com.wys.key.UUIDKeyGenerator;

public class DefaultKeyGenerator implements IKeyGenerator {

    @Override
    public String executeSql(String incrementerName) {
        return UUIDKeyGenerator.init(32, UUIDKeyGenerator.Type.bytes).generatorKey();
    }

    @Override
    public DbType dbType() {
        return DbType.KINGBASE_ES;
    }
}
