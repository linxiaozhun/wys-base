package com.wys.spring.swagger.plugin;


import springfox.bean.validators.plugins.Validators;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ParameterBuilderPlugin;
import springfox.documentation.spi.service.contexts.ParameterContext;

import javax.validation.constraints.NotBlank;
import java.util.Optional;


/**
 * @author XiaoLin
 * @date 2022年02月14日
 * @time 15:17
 */
public class NotBlankParameterBuilderPlugin implements ParameterBuilderPlugin{
    @Override
    public void apply(ParameterContext parameterContext) {
        Optional<NotBlank> notNull = Validators.annotationFromParameter(parameterContext, NotBlank.class);
        if (notNull.isPresent()) {
            parameterContext.requestParameterBuilder().required(true);
        }
    }

    @Override
    public boolean supports(DocumentationType documentationType) {
        return true;
    }
}
