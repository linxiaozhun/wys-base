package com.wys.spring.swagger.plugin;


import springfox.bean.validators.plugins.Validators;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.ParameterBuilderPlugin;
import springfox.documentation.spi.service.contexts.ParameterContext;

import javax.validation.constraints.NotNull;
import java.util.Optional;

/**
 * @author XiaoLin
 * @date 2022年02月14日
 * @time 14:55
 */
public class NotNullParameterBuilderPlugin implements ParameterBuilderPlugin{

    @Override
    public void apply(ParameterContext parameterContext) {
        Optional<NotNull> notNull = Validators.annotationFromParameter(parameterContext, NotNull.class);
        if (notNull.isPresent()) {
            parameterContext.parameterBuilder().required(true);
        }
    }

    @Override
    public boolean supports(DocumentationType documentationType) {
        return true;
    }


}
