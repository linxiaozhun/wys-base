package com.wys.spring.swagger.plugin;

import com.wys.spring.ApplicationContextHolder;
import com.wys.spring.SpringCommonProperties;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.mvc.condition.HeadersRequestCondition;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.OperationBuilderPlugin;
import springfox.documentation.spi.service.contexts.OperationContext;
import springfox.documentation.spring.web.WebMvcNameValueExpressionWrapper;

public class RequestHeaderBuilderPlugin implements OperationBuilderPlugin {

    @Override
    public void apply(OperationContext context) {
        SpringCommonProperties springCommonProperties = ApplicationContextHolder.context.getBean(SpringCommonProperties.class);
        if (ObjectUtils.isNotEmpty(springCommonProperties.getSwagger()) && StringUtils.isNotBlank(springCommonProperties.getSwagger().getHeaderNames())) {
            context.headers().addAll(WebMvcNameValueExpressionWrapper.from(new HeadersRequestCondition(springCommonProperties.getSwagger().getHeaderNames().split(",")).getExpressions()));
        }
    }

    @Override
    public boolean supports(DocumentationType delimiter) {
        return true;
    }
}
