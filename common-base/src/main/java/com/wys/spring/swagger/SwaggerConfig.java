package com.wys.spring.swagger;

import com.alibaba.nacos.common.utils.IPUtil;
import com.wys.spring.SpringCommonProperties;
import com.wys.spring.swagger.plugin.NotBlankModelPropertyBuilderPlugin;
import com.wys.spring.swagger.plugin.NotBlankParameterBuilderPlugin;
import com.wys.spring.swagger.plugin.NotNullModelPropertyBuilderPlugin;
import com.wys.spring.swagger.plugin.NotNullParameterBuilderPlugin;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.RequestParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

public class SwaggerConfig {

    @Bean
    NotNullParameterBuilderPlugin notNullParameterBuilderPlugin() {
        return new NotNullParameterBuilderPlugin();
    }

    @Bean
    NotNullModelPropertyBuilderPlugin notNullModelPropertyBuilderPlugin() {
        return new NotNullModelPropertyBuilderPlugin();
    }

    @Bean
    NotBlankModelPropertyBuilderPlugin notBlankModelPropertyBuilder() {
        return new NotBlankModelPropertyBuilderPlugin();
    }

    @Bean
    NotBlankParameterBuilderPlugin notBlankParameterBuilderPlugin() {
        return new NotBlankParameterBuilderPlugin();
    }



    @Bean
    @ConditionalOnMissingBean
    Docket docket(SpringCommonProperties springCommonProperties) {
            Docket docket = new Docket(ObjectUtils.isNotEmpty(springCommonProperties.getSwagger()) ? springCommonProperties.getSwagger().getDocumentationType().getDocumentationType() : DocumentationType.SWAGGER_2);
            List<RequestParameter> requestParameters = new ArrayList<>();
            if (ObjectUtils.isNotEmpty(springCommonProperties.getSwagger()) && StringUtils.isNotBlank(springCommonProperties.getSwagger().getHeaderNames())) {
                String[] headers = springCommonProperties.getSwagger().getHeaderNames().split(",");
                for (int i = 0; i < headers.length; i++) {
                    requestParameters.add(new RequestParameterBuilder().in(springCommonProperties.getSwagger().getParameterType()).name(headers[i]).required(true).description(headers[i]).parameterIndex(i).build());
                }
            }
            ApiInfo apiInfo = ObjectUtils.isNotEmpty(springCommonProperties.getSwagger().getApiInfo()) ? new ApiInfoBuilder().description(springCommonProperties.getSwagger().getApiInfo().getDescription())
                    .contact(new Contact("sunnykaka", "sunnykaka", "1977988429@qq.com"))
                    .title(springCommonProperties.getSwagger().getApiInfo().getTitle())
                    .version(springCommonProperties.getSwagger().getApiInfo().getVersion()).build() : ApiInfo.DEFAULT;
            docket.apiInfo(apiInfo)
                    .enable(true)
                    .select()
                    .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                    .paths(PathSelectors.any())
                    .build().globalRequestParameters(requestParameters)
                    .groupName(springCommonProperties.getSwagger().getGroupName())
                    .host(IPUtil.localHostIP());
            return docket;
    }


}
