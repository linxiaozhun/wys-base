package com.wys.spring;

import com.wys.spring.db.SpringMultipleDataSourceConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import(value = {SpringBaseApplicationConfig.class, SpringMultipleDataSourceConfiguration.class, OkHttpFeignLoadBalancerConfiguration.class})
@SpringBootApplication
public @interface EnableBaseSpringApplication {
}
