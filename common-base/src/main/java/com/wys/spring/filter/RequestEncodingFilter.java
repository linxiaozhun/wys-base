package com.wys.spring.filter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.web.servlet.filter.OrderedCharacterEncodingFilter;

public class RequestEncodingFilter extends OrderedCharacterEncodingFilter {

    private String encoding = "UTF-8";

    public RequestEncodingFilter(String encoding) {
        if (StringUtils.isNotBlank(encoding)) {
            this.encoding = encoding;
        }
        setEncoding(this.encoding);
        setForceRequestEncoding(true);
        setForceResponseEncoding(true);
    }

}
