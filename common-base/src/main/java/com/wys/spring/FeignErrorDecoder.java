package com.wys.spring;

import com.wys.api.common.R;
import com.wys.api.exception.BizException;
import com.wys.utils.JsonUtils;
import feign.Feign;
import feign.Response;
import feign.Util;
import feign.codec.ErrorDecoder;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class FeignErrorDecoder implements ErrorDecoder {
    @Override
    public Exception decode(String s, Response response) {
        R error;
        try {
            if (response.body() != null) {
                String body = Util.toString(response.body().asReader(StandardCharsets.UTF_8));
                if(StringUtils.isNotBlank(body)) {
                    try {
                        body = body.trim();
                        System.out.println("--------------异常响应消息:"+ body+"------inputStream:");
                       //error = JsonUtils.json2Object(body, R.class);
                    } catch (Exception ignore) {
                    }
                }
            }
        } catch (IOException ignored) {
        }
        return new BizException("远程服务异常");
    }
}
