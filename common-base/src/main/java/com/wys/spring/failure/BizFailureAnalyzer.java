package com.wys.spring.failure;

import com.wys.api.exception.BizException;
import org.springframework.boot.diagnostics.AbstractFailureAnalyzer;
import org.springframework.boot.diagnostics.FailureAnalysis;

public class BizFailureAnalyzer extends AbstractFailureAnalyzer<BizException> {
    @Override
    protected FailureAnalysis analyze(Throwable rootFailure, BizException cause) {
        return new FailureAnalysis("通用业务异常", cause.getMessage(), cause);
    }
}
