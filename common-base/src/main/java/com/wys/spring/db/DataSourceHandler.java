package com.wys.spring.db;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DataSourceHandler {

    private static final Logger logger = LoggerFactory.getLogger(DataSourceHandler.class);

    private static final ThreadLocal<String> threadLocal = ThreadLocal.withInitial(String::new);

    public static void putDataSource(String dataSourceName) {
        Assert.isTrue(StringUtils.isNotBlank(dataSourceName), "数据源名称不能为空");
        threadLocal.set(dataSourceName);
        logger.info(">>>>>>>>>>>>>>>当前数据源:{}", dataSourceName);
    }

    public static String getDataSource() {
        return threadLocal.get();
    }


    /**
     * 设置了数据源后一定要调用此方法进行移出
     */
    public static void removeDataSource() {
        threadLocal.remove();
    }


}
