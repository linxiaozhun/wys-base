package com.wys.spring.db;

import cn.hutool.db.sql.Condition;
import lombok.Getter;
import lombok.Setter;

public class ConditionX extends Condition {

    public static Builder builder = new Builder();


    /**
     * 构建sql查询语句条件 例如 name=xx and age=12 and sex=男
     *
     * @param filed    字段名
     * @param operator 操作类型例如： = <= >= !=
     * @param value    字段值
     * @param b        是否使用换位符
     */
    public ConditionX(String filed, String operator, String value, boolean b) {
        super(b);
        super.setField(filed);
        super.setOperator(operator);
        super.setValue(value);
    }

    @Getter
    @Setter
    public static class Builder {

        private String filed;

        private String operator;

        private String value;

        private boolean placeHolder = false;

        public Builder filed(String filed) {
            this.filed = filed;
            return this;
        }

        public Builder value(String value) {
            this.value = value;
            return this;
        }

        public Builder operator(String operator) {
            this.operator = operator;
            return this;
        }

        public Builder placeHolder(boolean placeHolder) {
            this.placeHolder = placeHolder;
            return this;
        }

        public ConditionX build() {
            return new ConditionX(filed, operator, value, placeHolder);
        }


    }


//    public static void main(String[] args) {
//        List<Object> list=new ArrayList<>();
//        list.add("xxx");
//        list.add("ffff0");
//        System.out.println(ConditionBuilder.of(new ConditionX("name", "=", "zhans", false),ConditionX.builder.filed("age1").value("hh1").operator("=").placeHolder(false).build(),ConditionX.builder.filed("age").value("hh").operator("=").placeHolder(false).build()).build(list));
//    }

}
