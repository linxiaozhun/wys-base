package com.wys.spring.db;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.aspectj.lang.annotation.Aspect;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataSourceInterceptor implements MethodInterceptor {


    @Nullable
    @Override
    public Object invoke(@NotNull MethodInvocation invocation) throws Throwable {
        DataSource dataSource = invocation.getMethod().getAnnotation(DataSource.class);
        DataSourceHandler.putDataSource(dataSource.name());
        Object obj = invocation
                .proceed();
        DataSourceHandler.removeDataSource();
        return obj;
    }
}
