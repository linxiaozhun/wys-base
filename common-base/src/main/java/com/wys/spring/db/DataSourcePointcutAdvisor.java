package com.wys.spring.db;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Advisor;
import org.springframework.aop.support.StaticMethodMatcherPointcutAdvisor;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.Method;

public class DataSourcePointcutAdvisor extends StaticMethodMatcherPointcutAdvisor {


    public DataSourcePointcutAdvisor (Advice advisor){
        this.setAdvice(advisor);
    }

    @Override
    public boolean matches(Method method, Class<?> targetClass) {
        if (method.isAnnotationPresent(DataSource.class)) {
            return true;
        }
        DataSource authorization = AnnotationUtils.findAnnotation(method, DataSource.class);
        if (ObjectUtils.isNotEmpty(authorization)) {
            return true;
        }
        return false;
    }
}
