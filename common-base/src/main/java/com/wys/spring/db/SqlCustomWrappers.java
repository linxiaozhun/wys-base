package com.wys.spring.db;

import cn.hutool.db.sql.ConditionBuilder;
import com.baomidou.mybatisplus.extension.toolkit.SqlRunner;
import com.wys.api.exception.BizException;
import com.wys.utils.JsonUtils;

import java.util.List;

public class SqlCustomWrappers {

    /**
     * 该类需要springBoot启动的情况下使用，直接在main方法下执行无法获取到sqlSessionFactory同时
     * 必须配置 global-config.enable-sql-runner=true
     */

    private static SqlRunner sqlRunner;

    public SqlCustomWrappers(Class<?> claas) {
        init(claas);
    }

    public static void init(Class<?> entity) {
        sqlRunner = SqlRunner.db(entity);
    }


    /**
     * 手写sql获取返回结果
     *
     * @param sql    sql语句 例如：select * from user where name={0} and sex={1} or age={3}...
     * @param source 需要转换的实体类
     * @param args   sql语句入参 和上面的{0},{1} 按顺序填写
     * @param <T>
     * @return
     */
    public static <T> List<T> selectList(String sql, Class<T> source, Object... args) {
        try {
            return JsonUtils.json2List(JsonUtils.object2Json(sqlRunner.selectList(sql, args)), source);
        } catch (Exception e) {
            throw new BizException("查询数据转换异常");
        }
    }

    /**
     * 基于 ConditionX组装的sql语句 例如
     * @param source 需要转换的class对象
     * @param conditionX new ConditionX("name", "=", "zhans", false), ConditionX.builder.filed("age1").value("hh1").operator("=").placeHolder(false).build()
     * @return
     * @param <T>
     */
    public static <T> List<T> selectList(Class<T> source, ConditionX... conditionX) {
        return selectList(ConditionBuilder.of(conditionX).build(), source);
    }


    /**
     * 获取单个实体对象
     *
     * @param sql    sql语句 例如：select * from user where name={0} and sex={1} or age={3}...
     * @param entity 需要转换的实体类对象
     * @param args
     * @param <T>
     * @return sql语句入参 和上面的{0},{1} 按顺序填写
     */
    public static <T> T selectOne(String sql, Class<T> entity, Object... args) {
        return JsonUtils.json2Object(JsonUtils.object2Json(sqlRunner.selectOne(sql, args)), entity);
    }

    /**
     * 手写sql插入语句
     *
     * @param sql  insert into user (name,sex,age)values({0},{1},{2})
     * @param args sql语句入参 和上面的{0},{1} 按顺序填写
     * @return
     */
    public static boolean insert(String sql, Object... args) {
        return sqlRunner.insert(sql, args);
    }

    public static SqlRunner getSqlRunner() {
        return sqlRunner;
    }
}
