package com.wys.spring.db;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public class DynamicDataSource extends AbstractRoutingDataSource {

    private final MultipleDataSourceConfig multipleDataSourceConfig;

    public DynamicDataSource(MultipleDataSourceConfig multipleDataSourceConfig) {
        this.multipleDataSourceConfig = multipleDataSourceConfig;
    }

    @Override
    protected Object determineCurrentLookupKey() {
        return StringUtils.isNotBlank(DataSourceHandler.getDataSource()) ? DataSourceHandler.getDataSource() : multipleDataSourceConfig.getDataSource().get("master");
    }


}
