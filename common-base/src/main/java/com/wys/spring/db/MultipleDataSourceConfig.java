package com.wys.spring.db;

import com.alibaba.druid.pool.DruidDataSource;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.annotation.Order;

import java.util.Map;

@ConfigurationProperties(prefix = "multiple")
@Getter
@Setter
@Order(Integer.MIN_VALUE)
public class MultipleDataSourceConfig {

    /**
     * 多数据源map
     */
    private Map<String, DruidDataSource> dataSource;

    /**
     * 默认开启数据源刷新
     */
    private boolean enableRefresh = true;


}
