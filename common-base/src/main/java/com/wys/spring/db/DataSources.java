package com.wys.spring.db;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataSources {

    private String url;

    private String driverClassName;

    private String username;

    private String password;

}
