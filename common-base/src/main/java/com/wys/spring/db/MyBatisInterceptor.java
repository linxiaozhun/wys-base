package com.wys.spring.db;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Invocation;

import java.util.ArrayList;
import java.util.List;

public interface MyBatisInterceptor {

   List<Interceptor> applyIntercept();

    class DefaultMyBatisInterceptor implements MyBatisInterceptor{

        @Override
        public List<Interceptor> applyIntercept() {
            return new ArrayList<>();
        }
    }

}
