package com.wys.spring.log;


import cn.hutool.core.util.ObjectUtil;
import com.wys.utils.JsonUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Request;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpRequest;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: people-defence-service
 * @description: 请求日志类
 * @author: xiaoLin
 * @create: 2021-08-24 08:58
 **/
@Getter
@Setter
@Slf4j
@ToString
public class RequestLog {

    private String httpMethod;

    private String url;

    private Object body;

    private Map param;

    private Map heard;

    private String queryString;

    private Map<String, Object> cookies;


    public static RequestLog init(String method, String url, Object body) {
        RequestLog requestLog = new RequestLog();
        requestLog.setBody(body);
        requestLog.setHttpMethod(method);
        requestLog.setUrl(url);
        return requestLog;
    }


    public static RequestLog createRequestLog(HttpRequest httpServletRequest, Object body, HttpServletResponse response) throws IOException {
        RequestLog requestLog = new RequestLog();
        requestLog.setUrl(httpServletRequest.getURI().toString());
        builderRequestBody(body, requestLog);
        return requestLog;
    }

    private static void builderRequestBody(Object body, RequestLog requestLog) {
        try {
            if (ObjectUtil.isNotNull(body)) {
                if (body.toString().startsWith("[") || body.toString().startsWith("{")) {
                    requestLog.setBody(JsonUtils.json2Object(body.toString(), Object.class));
                } else {
                    requestLog.setBody(body);
                }
            }
        } catch (Exception e) {
            log.error("json数据解析异常:", e);
        }
    }

    public static RequestLog createRequestLog(HttpServletRequest httpServletRequest, Object body, HttpServletResponse response) throws IOException {
        RequestLog requestLog = new RequestLog();
        String uri = "http://" + httpServletRequest.getHeader("host").concat(StringUtils.isNoneBlank(httpServletRequest.getRequestURI()) ? httpServletRequest.getRequestURI() : httpServletRequest.getPathInfo());
        requestLog.setHttpMethod(httpServletRequest.getMethod());
        requestLog.setUrl(uri);
        requestLog.setQueryString(httpServletRequest.getQueryString());
        builderRequestBody(body, requestLog);
        requestLog.setParam(httpServletRequest.getParameterMap());
        Map<String, Object> heard = new HashMap<>();
        Enumeration<String> enumeration = httpServletRequest.getHeaderNames();
        while (enumeration.hasMoreElements()) {
            String heardName = enumeration.nextElement();
            if (heardName.equals("content-type") || heardName.equals("connection") || heardName.equals("host") || heardName.equals("referer") || heardName.endsWith("token") || heardName.endsWith("sign")) {
                heard.put(heardName, httpServletRequest.getHeader(heardName));
            }
        }
        if (StringUtils.isNoneBlank(httpServletRequest.getQueryString()) && StringUtils.isNoneBlank(uri)) {
            requestLog.setUrl(uri.concat(httpServletRequest.getQueryString()));
        }
        if (ObjectUtil.isNotEmpty(httpServletRequest.getCookies())) {
            Map<String, Object> cookieMap = new HashMap<>();
            for (Cookie cookie : httpServletRequest.getCookies()) {
                cookieMap.put(cookie.getName(), cookie.getValue());
            }
            requestLog.setCookies(cookieMap);
        }
        requestLog.setHeard(heard);
        return requestLog;
    }



}
