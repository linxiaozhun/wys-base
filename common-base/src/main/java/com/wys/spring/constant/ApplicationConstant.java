package com.wys.spring.constant;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;


@Getter
@Setter
public class ApplicationConstant {

    @Value("${spring.profiles.active}")
    private String profile;


}
