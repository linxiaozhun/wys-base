package com.wys.spring.cache;


import com.wys.api.common.R;
import com.wys.utils.JsonUtils;
import com.wys.utils.password.DESUtil;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;

@Aspect
public class DataCacheInterceptor {


    private static final String PREFIX = "#";


    @Around(value = "@annotation(dataCache)")
    public Object around(ProceedingJoinPoint proceedingJoinPoint, DataCache dataCache) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();
        Method method = methodSignature.getMethod();
        if (method != null) {
            StringBuilder stringBuilder = new StringBuilder(StringUtils.isNotBlank(dataCache.value()) ? dataCache.value() : method.getDeclaringClass().getName().concat(PREFIX).concat(method.getName()));
            for (Object arg : proceedingJoinPoint.getArgs()) {
                stringBuilder.append(DESUtil.SHA256(JsonUtils.object2Json(arg))).append(PREFIX);
            }
            String key = stringBuilder.substring(0, stringBuilder.lastIndexOf(PREFIX));
            if (CacheContext.existKey(dataCache.grade(), key)) {
                return CacheContext.getData(dataCache.grade(), key,method.getReturnType());
            }
            CacheContext.putData(dataCache.grade(), key, proceedingJoinPoint.proceed());
        }
        return proceedingJoinPoint.proceed();
    }


}
