package com.wys.spring.cache;

import com.wys.api.common.R;
import com.wys.spring.ApplicationContextHolder;
import com.wys.utils.JsonUtils;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public class RedisCacheStrategy implements CacheStrategy<Object>{

   private StringRedisTemplate stringRedisTemplate;

   private CacheGrade cacheGrade;

   public RedisCacheStrategy(CacheGrade cacheGrade){
       this.stringRedisTemplate= ApplicationContextHolder.context.getBean(StringRedisTemplate.class);
       this.cacheGrade=cacheGrade;
   }


    @Override
    public void saveData(Object o, String key) {
       stringRedisTemplate.boundValueOps(key).set(JsonUtils.object2Json(o),1, TimeUnit.DAYS);
    }




    @Override
    public <F> Object getCacheData(String key,Class<F> rClass) {
        return JsonUtils.json2Object(stringRedisTemplate.boundValueOps(key).get(), R.class);
    }

    @Override
    public CacheGrade getCacheGrade() {
        return cacheGrade;
    }

    @Override
    public boolean existKey(String key) {
        return stringRedisTemplate.hasKey(key);
    }
}
