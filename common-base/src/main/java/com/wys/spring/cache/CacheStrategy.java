package com.wys.spring.cache;

import java.util.concurrent.ExecutionException;

public interface CacheStrategy<T> {

    void saveData(T t, String key);

    <F> T getCacheData(String key,Class<F> rClass) throws ExecutionException;

    CacheGrade getCacheGrade();

    boolean existKey(String key);

}
