package com.wys.spring.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.wys.utils.BeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class SystemCacheStrategy implements CacheStrategy<Object> {

    private final static Logger logger = LoggerFactory.getLogger(SystemCacheStrategy.class);


    private static final LoadingCache<String, Object> hashMap = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.DAYS).build(new CacheLoader<String, Object>() {
        @Override
        public Object load(String s) throws Exception {
            return "找不到：" + s + " key";
        }
    });


    private CacheGrade cacheGrade;

    public SystemCacheStrategy(CacheGrade cacheGrade) {
        this.cacheGrade = cacheGrade;
    }

    @Override
    public void saveData(Object o, String key) {
        hashMap.put(key, o);
    }

    @Override
    public <F> Object getCacheData(String key, Class<F> fClass) throws ExecutionException {
        return BeanUtil.from(hashMap.get(key), fClass);
    }

    @Override
    public CacheGrade getCacheGrade() {
        return cacheGrade;
    }

    @Override
    public boolean existKey(String key) {
        return !ObjectUtils.isEmpty(hashMap.getUnchecked(key));
    }
}
