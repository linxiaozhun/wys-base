package com.wys.spring.cache;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class CacheContext {

    private static final List<CacheStrategy> cacheStrategies = Arrays.asList(new CacheStrategy[]{new SystemCacheStrategy(
            CacheGrade.SYSTEM
    ), new RedisCacheStrategy(CacheGrade.REDIS)});

    public static void putData(CacheGrade cacheGrade, String key, Object value) {
        for (CacheStrategy cacheStrategy : cacheStrategies) {
            if (cacheGrade.equals(cacheStrategy.getCacheGrade())) {
                cacheStrategy.saveData(value, key);
            }
        }
    }

    public static <F>Object getData(CacheGrade cacheGrade, String key,Class<F> fClass) throws ExecutionException {
        for (CacheStrategy cacheStrategy : cacheStrategies) {
            if (cacheStrategy.getCacheGrade().equals(cacheGrade)) {
                return cacheStrategy.getCacheData(key,fClass);
            }
        }
        return null;
    }

    public static boolean existKey(CacheGrade cacheGrade, String key) {
        for (CacheStrategy strategy : cacheStrategies) {
            if (strategy.getCacheGrade().equals(cacheGrade)) {
                return strategy.existKey(key);
            }
        }
        return false;
    }

    public static <T> void registerCacheStrategy(CacheStrategy<T> cacheStrategy) {
        cacheStrategies.add(cacheStrategy);
    }


}
