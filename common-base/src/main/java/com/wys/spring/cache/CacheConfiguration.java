package com.wys.spring.cache;

import org.springframework.context.annotation.Bean;

public class CacheConfiguration {

    @Bean
    public DataCacheInterceptor dataCacheInterceptor() {
        return new DataCacheInterceptor();
    }

}
