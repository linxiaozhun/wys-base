package com.wys.spring.cache;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DataCache {

    @AliasFor("name")
    String value() default "";

    @AliasFor("value")
    String name() default "";

    CacheGrade grade() default CacheGrade.SYSTEM;

}
