package com.wys.spring.health;

import com.wys.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;

public class SpringEmptyHealthIndicator extends AbstractHealthIndicator {

    private static final Logger logger = LoggerFactory.getLogger(SpringEmptyHealthIndicator.class);

    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        builder.up().build();
        logger.warn("《《《《《《《《《《《《《《《《《开始检测数据源连接》》》》》》》》》》》》》》》》》》");
    }
}
