package com.wys.spring.health;

import org.springframework.boot.actuate.autoconfigure.health.CompositeHealthContributorConfiguration;
import org.springframework.boot.actuate.health.HealthContributor;
import org.springframework.boot.actuate.redis.RedisHealthIndicator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class RedisHealthContributorConfiguration extends CompositeHealthContributorConfiguration<RedisHealthIndicator, RedisConnectionFactory> {

    private final Map<String, RedisConnectionFactory> redisConnectionFactoryMap;

    public RedisHealthContributorConfiguration(Map<String, RedisConnectionFactory> redisConnectionFactoryMap) {
        this.redisConnectionFactoryMap = filterRedisConnectionFactory(redisConnectionFactoryMap);
    }

    public Map<String, RedisConnectionFactory> filterRedisConnectionFactory(Map<String, RedisConnectionFactory> redisConnectionFactoryMap) {
        Map<String, RedisConnectionFactory> map = new ConcurrentHashMap<>();
        if (redisConnectionFactoryMap != null) {
            for (Map.Entry<String, RedisConnectionFactory> stringRedisConnectionFactoryEntry : redisConnectionFactoryMap.entrySet()) {
                RedisConnectionFactory value = stringRedisConnectionFactoryEntry.getValue();
                if (value != null) {
                    if (value instanceof JedisConnectionFactory) {
                        JedisConnectionFactory jedisConnectionFactory = (JedisConnectionFactory) value;
                        if (jedisConnectionFactory.getHostName().equalsIgnoreCase("localhost") || jedisConnectionFactory.getHostName().startsWith("127.0.0.1")) {
                            continue;
                        }
                    } else if (value instanceof LettuceConnectionFactory) {
                        LettuceConnectionFactory lettuceConnectionFactory = (LettuceConnectionFactory) value;
                        if (lettuceConnectionFactory.getHostName().equalsIgnoreCase("localhost") || lettuceConnectionFactory.getHostName().startsWith("127.0.0.1")) {
                            continue;
                        }
                    }
                    map.put(stringRedisConnectionFactoryEntry.getKey(), stringRedisConnectionFactoryEntry.getValue());
                }
            }
        }
        return map;
    }


    @Bean
    @ConditionalOnMissingBean(name = {"redisHealthIndicator", "redisHealthContributor"})
    public HealthContributor redisHealthContributor(Map<String, RedisConnectionFactory> redisConnectionFactories) {
        if (redisConnectionFactoryMap == null || redisConnectionFactoryMap.isEmpty()) {
            return new SpringEmptyHealthIndicator();
        }
        return this.createContributor(redisConnectionFactories);
    }


}
