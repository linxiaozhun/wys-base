package com.wys.spring.mqtt;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@Getter
@Setter
@ConfigurationProperties(prefix = "emqx")
public class EMQXProperties {

    /**
     * 初始化默认取第一个，后续的用来链接多mqtt服务器
     */
    private String[] host;

    private String clientId;

    private String userName;

    private String password;

    private Boolean automaticReconnect;

    private int connectionTimeout;

    /**
     * 设置客户端与服务端的心跳间隔
     */
    private int keepAliveInterval;

    @NestedConfigurationProperty
    private Will will;


    @Getter
    @Setter
    public static class Will {
        /**
         * 设置最后主题
         */
        private String topic;

        /**
         * 内容
         */
        private String payload;

        /**
         * 消息安全等级 0,1,2
         */
        private int qos;

        /**
         * 是否持久话此条消息
         */
        private Boolean retained;

    }


}
