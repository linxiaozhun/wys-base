package com.wys.spring.controller.advice;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.wys.api.exception.BizException;
import com.wys.spring.SpringCommonProperties;
import com.wys.utils.password.DESUtil;
import com.wys.utils.password.RSAUtils;
import org.apache.commons.io.IOUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

@RestControllerAdvice(annotations = {RestController.class, Controller.class})
public class ControllerRequestBodyAdvice implements RequestBodyAdvice {

    private static final Logger logger = LoggerFactory.getLogger(ControllerRequestBodyAdvice.class);

    private SpringCommonProperties.ResponseEncrypt responseEncrypt;

    private static final String DES3_KEY = "sunnykakaking";


    public ControllerRequestBodyAdvice(SpringCommonProperties.ResponseEncrypt responseEncrypt) {
        this.responseEncrypt = responseEncrypt;
    }

    @Override
    public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return true;
    }

    @Override
    public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
        if (!responseEncrypt.isEnable()) {
            return inputMessage;
        }
        HttpServletRequest servletServerHttpRequest = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        if (ObjectUtils.isNotEmpty(inputMessage.getBody())) {
            if (responseEncrypt.getExcludeUrls() != null && responseEncrypt.getExcludeUrls().length() > 0) {
                logger.warn("Request Data Encrypt ExcludeUrl:{}, Request Url Path:{}", responseEncrypt.getExcludeUrls(), servletServerHttpRequest.getRequestURI());
                for (String s : responseEncrypt.getExcludeUrls().split(",")) {
                    if (servletServerHttpRequest.getRequestURI().startsWith(s.replace("/**", ""))) {
                        return inputMessage;
                    } else {
                        return getHttpInputMessage(inputMessage);
                    }
                }
            } else {
                return getHttpInputMessage(inputMessage);
            }

        }
        return inputMessage;
    }

    @NotNull
    private HttpInputMessage getHttpInputMessage(HttpInputMessage inputMessage) {
        switch (responseEncrypt.getEncryptModel()) {
            case RSA:
                return new HttpInputMessage() {
                    @Override
                    public InputStream getBody() throws IOException {
                        String pwdBody = IOUtils.toString(inputMessage.getBody(), StandardCharsets.UTF_8).replace("\"", "");
                        logger.warn("RSA decrypt before data:{}", pwdBody);
                        String body = RSAUtils.decryptStr(pwdBody);
                        logger.warn("RSA decrypt after data:{}", body);
                        return IOUtils.toInputStream(body);
                    }

                    @Override
                    public HttpHeaders getHeaders() {
                        return inputMessage.getHeaders();
                    }
                };
            case DES3:
                return new HttpInputMessage() {
                    @Override
                    public InputStream getBody() throws IOException {
                        String pwdBody = IOUtils.toString(inputMessage.getBody(), StandardCharsets.UTF_8).replace("\"", "");
                        logger.warn("3DES decrypt before data:{}", pwdBody);
                        String body = DESUtil.decrypt3Des(DES3_KEY, pwdBody);
                        logger.warn("3DES decrypt after data:{}", body);
                        return IOUtils.toInputStream(Objects.requireNonNull(body));
                    }

                    @Override
                    public HttpHeaders getHeaders() {
                        return inputMessage.getHeaders();
                    }
                };
            default:
                throw new BizException("存在未知的解密策略");

        }
    }

    @Override
    public Object afterBodyRead(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return body;
    }

    @Override
    public Object handleEmptyBody(Object body, HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return body;
    }
}
