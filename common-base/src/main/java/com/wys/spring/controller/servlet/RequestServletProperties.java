package com.wys.spring.controller.servlet;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestServletProperties {

    /*
      通过","分隔 例如 /uri/text,/url/img
     */
    private String excludePaths;

}
