package com.wys.spring.redis;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import org.aopalliance.aop.Advice;
import org.jetbrains.annotations.NotNull;
import org.springframework.aop.support.StaticMethodMatcherPointcutAdvisor;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.Method;

public class RedisInterceptor extends StaticMethodMatcherPointcutAdvisor {

    public RedisInterceptor(Advice advice) {
        this.setAdvice(advice);
    }

    @Override
    public boolean matches(Method method, @NotNull Class<?> targetClass) {
        return method.getAnnotation(RedisLock.class) != null || ObjectUtils.isNotEmpty(AnnotationUtils.findAnnotation(method, RedisLock.class));
    }
}
