package com.wys.spring.redis;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisLock {

    /**
     * redis的key值 例如 任务执行中、当前锁已存在等提示语
     *
     * @return
     */
    String value() default "";

    /**
     * redis的key
     *
     * @return
     */
    String name() default "";

    /**
     * 超时时间 单位：秒  默认永久
     *
     * @return
     */
    long outTime() default -1;


}
