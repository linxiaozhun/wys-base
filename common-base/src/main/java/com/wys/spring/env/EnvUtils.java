package com.wys.spring.env;

import com.wys.spring.ApplicationContextHolder;
import org.springframework.boot.context.properties.bind.Bindable;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.core.env.ConfigurableEnvironment;

import java.util.Map;

public class EnvUtils {

    private static ConfigurableEnvironment configurableEnvironment;

    public static void init() {
        configurableEnvironment = ApplicationContextHolder.context.getBean(ConfigurableEnvironment.class);
    }

    public static String getProperty(String key) {
        return configurableEnvironment.getProperty(key);
    }

    /**
     * 模糊匹配application.yml中的配置项
     * @param key 配置文件中的key名
     * @param tClass 需要转换的value值
     * @return 返回map
     * @param <T> 类泛型
     */
    public static <T> Map<String, T> matchingKey(String key, Class<T> tClass) {
        return Binder.get(configurableEnvironment).bind(key, Bindable.mapOf(String.class, tClass)).get();
    }


}
