package com.wys.spring;

public class RequestBodyHandler {

    private static final ThreadLocal<Object> requestThreadLocal = ThreadLocal.withInitial(Object::new);

    public static void setRequestBody(Object obj) {
        requestThreadLocal.set(obj);
    }

    public static Object getRequestBody() {
        return requestThreadLocal.get();
    }


    public static void removeAll() {
        requestThreadLocal.remove();

    }

}
