package com.wys.spring;

import com.wys.spring.controller.servlet.RequestServletProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import springfox.documentation.service.ParameterType;
import springfox.documentation.spi.DocumentationType;

@Getter
@Setter
@ConfigurationProperties(prefix = "wys")
public class SpringCommonProperties {

    @NestedConfigurationProperty
    private ResponseEncrypt responseEncrypt;

    @NestedConfigurationProperty
    private RequestServletProperties requestServletProperties;

    @NestedConfigurationProperty
    private Logging logging;

    @NestedConfigurationProperty
    private Swagger swagger;

    @Getter
    @Setter
    public static class Logging {

        /**
         * 开启请求日志 默认开启
         */
        private Boolean requestEnable = true;

        /**
         * 开启响应日志 默认开启
         */
        private Boolean responseEnable = true;

    }

    @Getter
    @Setter
    public static class Swagger {

        /**
         * 添加swagger全局headerName 可以根据','分割多个
         */
        private String headerNames;

        /**
         * 全局参数类型 如：header、body、param等
         */
        private ParameterType parameterType;

        /**
         * 分组名称
         */
        private String groupName;

        /**
         * 文档类型 swagger2.0 swagger2.1 ...
         */
        private DocType documentationType = DocType.swagger2;

        /**
         * 文档主体内容
         */
        private ApiInfo apiInfo;


    }


    public enum DocType {
        swagger1_2(DocumentationType.SWAGGER_12), swagger2(DocumentationType.SWAGGER_2), swagger3(DocumentationType.OAS_30);

        private DocumentationType documentationType;

        DocType(DocumentationType documentationType) {
            this.documentationType = documentationType;
        }

        public DocumentationType getDocumentationType() {
            return documentationType;
        }
    }

    @Getter
    @Setter
    public static class ApiInfo {

        /**
         * 文档标题
         */
        private String title;
        /**
         * 文档备注
         */
        private String description;

        /**
         * 文档版本
         */
        private String version;
    }

    @Getter
    @Setter
    public static class ResponseEncrypt {

        /**
         * 是否开启api数据服务加密
         */
        private boolean enable;

        /**
         * 加密策略 AES,RSA,3DES
         */
        private EncryptModel encryptModel;

        /**
         * 不需要加密的api路径
         */
        private String excludeUrls;

        public enum EncryptModel {
            RSA, DES, DES3, SHA256
        }

    }

}
