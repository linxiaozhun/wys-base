package com.wys.spring;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @Author liubin
 * @Date 2017/5/15 15:03
 */
public class ApplicationContextHolder implements ApplicationContextAware {

    public volatile static ApplicationContext context;


    private static AtomicBoolean startup = new AtomicBoolean(false);

    public static final ApplicationContextHolder INSTANCE = new ApplicationContextHolder();

    private ApplicationContextHolder() {
    }

    public static ApplicationContextHolder getInstance() {
        return INSTANCE;
    }


    public static boolean tryStartup() {
        return startup.compareAndSet(false, true);
    }

    public static boolean isStartup() {
        return startup.get();
    }

    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
        context=applicationContext;
    }
}
