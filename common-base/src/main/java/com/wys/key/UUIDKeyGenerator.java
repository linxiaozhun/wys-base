package com.wys.key;

import org.springframework.security.crypto.codec.Hex;
import org.springframework.security.crypto.keygen.KeyGenerators;

public class UUIDKeyGenerator {
    private final int keyLength;

    private final Type type;

    private static UUIDKeyGenerator uuidKeyGenerator;

    public UUIDKeyGenerator(int keyLength, Type type) {
        this.keyLength = keyLength;
        this.type = type;
    }

    public int getKeyLength() {
        return keyLength;
    }

    /**
     *
     * @param keyLength key生成所需的长度
     * @param type key生成的模式类型
     * @return
     */
    public static UUIDKeyGenerator init(int keyLength, Type type) {
        uuidKeyGenerator = new UUIDKeyGenerator(keyLength, type);
        return uuidKeyGenerator;
    }

    public  String generatorKey() {
        if ("byte".equals(uuidKeyGenerator.type.name())) {
            return new String(Hex.encode(KeyGenerators.secureRandom(uuidKeyGenerator.keyLength).generateKey()));
        } else {
            return new String(Hex.encode(KeyGenerators.shared(uuidKeyGenerator.keyLength).generateKey()));
        }
    }

    public enum Type {
        bytes, shared;
    }


//    public static void main(String[] args) {
//
//        System.out.println(UUIDKeyGenerator.init(16,Type.bytes).generatorKey());;
//
//    }

}
