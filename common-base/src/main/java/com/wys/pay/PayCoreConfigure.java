package com.wys.pay;

import com.alipay.api.AlipayApiException;
import com.wys.pay.alipay.AlipayCore;
import com.wys.pay.alipay.entity.AlipayConfig;
import com.wys.pay.wxpay.Wxpay3Core;
import com.wys.pay.wxpay.entity.WxpayConfig;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@ConfigurationPropertiesScan
@EnableConfigurationProperties(value = {AlipayConfig.class,WxpayConfig.class})
public class PayCoreConfigure {

    @Bean
    public AlipayConfig alipayConfig() {
        return new AlipayConfig();
    }

    @Bean
    @ConditionalOnProperty(prefix = "alipay",name = "enable",havingValue = "true")
    public AlipayCore alipayCore() throws AlipayApiException {
        return new AlipayCore(this.alipayConfig());
    }

    @Bean
    public WxpayConfig wxpayConfig(){
        return new WxpayConfig();
    }

    @Bean
    @ConditionalOnProperty(prefix = "wxpay",name = "enable",havingValue = "true")
    public Wxpay3Core wxpay3Core(){
        return new Wxpay3Core(wxpayConfig());
    }


}
